# Fw Duck Food Sessions

## Approach

First of all, we need to take care of the problem itself, questions to solve and some ways to add information inside the system to have sure to reach this information.

After reach, this conclusion, choosing the technologies should be the next step.

Q1- What time ducks are fed?
It can happen in basically in three ways: 1- Manually, comes when the user input the value in the interface; 3- Repeated, when the user set it manually fed as a preset in the interface.

The option to cover both states comes to manually input. The interface should have an option to flag the input as a normal input or repeated every day.

In the interface, a component comes with now as default moment. The user can change it as it whants back and forward. In the end, the user can check if it will be flagged a repeatable task or not.

Q2- What food the ducks are fed?
Q5- What kind of food dusk are fed?
Q6- How much food the ducks are fed?

The duck's food is completely tied with the kind of this food and also
with the amount of food. Searching a bit about the duck's diet, we can assume they have a variety of food to eat, simplifying it, we will separate them kind of food in two types: 1- Ration, the processed food to breed ducks, with all nutrients needed, factory-made; and 2- Natural, food that's are part of ducks the natural duck ecosystem.

Enumerating types of food for these two kinds, adding for the first one: a)Basic Ration; b)Ration for growing; c)Ration for breeding. And for the natural type: a)Small fish or fish eggs; b)Snails, worms, slugs, or mollusks; c)Grass, leaves, and weeds; d)Algae or aquatic plants or roots;
e)Frogs, tadpoles, salamanders, or other amphibians; f)Aquatic or land insects; g)Seeds or grains; h)Small berries, fruits, or nuts. In both options is easy to build a nutritional table for each feed session. The quantity of food can be added in portions separated for each 100g.

To add this, the interface needs to have three fields, one asking about the amount of food, another about the kind of food and another asking about the type of food. The first field will ask about the amount of food, a simple text field can take care of it, just accepting numbers and with a 100 stepper. A selector appears in the second field, the user can change between options of kind of food, if the user did not have sure, will have an option for it. Also, the third field will have a select, the values can vary based on the first option selected. All values will have an error message for it. All the food information will go to the cloud in a separated object.

Q3- Where the ducks are fed?
Enumerate all the parks around the world it's a hard job, we probably need a lot of information and a lot of people to input it. The first move to add parks in the system should be asking the name of the park.

We can let a future implementation at this point, to know more information about the parks. For now, focusing on the duck's diet and the people's actions will be the objective.

For just the park name, the interface will have a single text input field.

The park name will go directly to the cloud as a simple string.

Q4- How many ducks are fed?
When the people are in a lake happily throwing food to ducks they are not concerned exactly in with how much ducks are in the lake. Searching in the google images in a while, we can understand that ducks live in portions between 2 at 15.

To simplify the data and reduce the complexity of the app, the portion of ducks can be structured in each 3. In this way, is easy to figure out the difference between the portions and delivering more assertive quantity.

In the interface, a simple number field with a stepper will send the number directly to the cloud as an integer. Negative numbers will produce an interface error.

## Tech Stack

To deal with the javascript itself, babel will be nice, since the faster and more readable way is writing Modern Javascript (es6) instead of pure javascript. Babel also is a nice option to deliver it in a NodeJS environment and also in the browser. And is connected to a very different type of plugins and options. Also using Eslint with airbnb, js-a11y and pretify helps a lot to deal with the code.

To deal in the frontend ReactJS is a nice option, It can follow a nice design pattern and it has too many integrations with various different systems, like, the rest of the tech stack in this document.

To deal with the css, CSS-In-JS is a nice approach to, to do this JSS and sagira-jss is a nice approach to make the basic job for fast apps, JSS has an easy way to integrate CSS-in-JS in the application and sagira-jss can take care of the grid system and, basic reset and a different approach in the mobile metric system.

For the cloud, in the past, the best option should be an AWSGatewayAPI, using AWSLambda functions as a proxy with GraphQL endpoint inside of it, interfacing the information between an AWSDynamoDB instance. After research, it's a bit outdated.

Now, AWSAppSync comes with a really nice approach to deal with this kind of information. It is a new service using GraphQL as an interface for too many different services, and the Amazon traditional VTL(Velocity Template Language). It replaces the description above and beyond.

For tests, E2E is coming first in this project, using Jest and Puppeteer. The goals are divided by files, testing access, navigation and also fill the session form. All with screenshots. Tests are considered to run locally in a external endopint, but it can be changed to locally runnig webpack-dev-server at same time.

## Component Diagram

[Imgur](https://i.imgur.com/6lLRwgW.png)

This is just a simple diagram to understand how the connections between components, can be accessed here: https://realtimeboard.com/app/board/o9J_kxjSVBE=/

## folder structure

### the `/`

This directory carries the most external files to the system, between configurations and CI environment.

### the `/public`

This folder contains fonts, a simple HTML to starts the React SPA. It also contains a file with all page content, it works like a simple "deploy oriented" i18n support, it's not the best, but it works well when are you talking about big language dictionaries in React Native apps.

### the principal `/src`

This folder contains the principal code of the system, regardless of the kind.
The subfolders inside of it can vary between projects, in this case, it has:
`./app-sync` - Folder, holding the principal files for AWSAppSync structure, inside of it, GraphQL and VTL languages;
`./client` - It contains 3 principal things, the starting file, the router, and the context file, inside of it, just Javascript;
`./components` - The main content of the system. It carries the components for all visual objects and pages, inside of it, just Javascript;
`./data` - The system information hub, inside of it, GraphQL and Javascript files;
`./styles` - The fundamentals of styles in the system, inside of it, just Javascript;
`./utils` - Helper modules to help in other modules;

### `/webpack` folder

All webpack configuration files, in this project, are just used two scenarios. It's expansible and, add a new scenario in the future should be very easy.
