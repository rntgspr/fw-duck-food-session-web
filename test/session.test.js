/* eslint-disable no-undef */
import '@babel/polyfill';
import { apiUrl, endpoint, screenshotsDir } from '../src/config';

describe('Session', () => {
  beforeAll(async () => {
    await page.setViewport({ width: 375, height: 748, });
  });

  it('Should return the session page', async () => {
    await page.goto(`${endpoint}/new-session`, { waitUntil: 'networkidle0', });
    await page.screenshot({ path: `${screenshotsDir}/session.0.jpg`, });

    const textToMath = 'Wich park you fed the ducks?';
    const h3 = await page.evaluate(() => {
      return Array.from(document.getElementsByTagName('h3')).map(a => a.innerHTML);
    });

    await expect(h3[0]).toMatch(textToMath);
  }, 10000);

  it('Fill a feed session form', async () => {
    await page.goto(`${endpoint}/new-session`, { waitUntil: 'networkidle0', });
    await page.type('input[name="park"]', 'Park Test');
    await page.type('input[name="totalDucks"]', '6');
    await page.type('input[name="foodAmount"]', '100');
    await page.select('select[name="foodKind"]', 'RATION');
    await page.select('select[name="foodTypeRation"]', 'BASIC_RATION');
    await page.screenshot({ path: `${screenshotsDir}/session.1.jpg`, });
    await page.click('button[name="sendSession"]');

    await page.waitForResponse(response => response.url() === apiUrl && response.status() === 200);

    await page.screenshot({ path: `${screenshotsDir}/session.2.jpg`, });

    const textToMath = 'You fed 6 ducks in Park Test, well done!';
    const h1 = await page.evaluate(() => {
      return Array.from(document.getElementsByTagName('h1')).map(a => a.innerHTML);
    });

    await expect(h1[0]).toMatch(textToMath);
  }, 10000);

  it('Should navigate from Session page to Home', async () => {
    await page.goto(`${endpoint}/new-session`);
    await page.click('a[href="/"]');

    const location = await page.evaluate(() => {
      return document.location;
    });

    expect(location.href).toContain('/');
    expect(location.pathname).toMatch('/');
  }, 10000);
});
