/* eslint-disable no-undef */
import '@babel/polyfill';
import { endpoint, screenshotsDir } from '../src/config';

describe('Home', () => {
  beforeAll(async () => {
    await page.setViewport({ width: 375, height: 748, });
  });

  it('Should return the home page', async () => {
    await page.goto(`${endpoint}/`, { waitUntil: 'networkidle0', });
    await page.screenshot({ path: `${screenshotsDir}/home.0.jpg`, });

    const textToMath = 'Welcome to Duck Feeding App!';
    const h1 = await page.evaluate(() => {
      return Array.from(document.getElementsByTagName('h1')).map(a => a.innerHTML);
    });

    await expect(h1[0]).toMatch(textToMath);
  }, 10000);

  it('Should navigate from Home to Reports page', async () => {
    await page.goto(`${endpoint}/`, { waitUntil: 'networkidle0', });
    await page.click('a[href="/reports"]');

    const location = await page.evaluate(() => {
      return document.location;
    });

    expect(location.href).toContain('/reports');
    expect(location.pathname).toMatch('/reports');
  }, 10000);

  it('Should navigate from Home to Fed Ducks page', async () => {
    await page.goto(`${endpoint}/`, { waitUntil: 'networkidle0', });
    await page.click('a[href="/new-session"]');

    const location = await page.evaluate(() => {
      return document.location;
    });

    expect(location.href).toContain('/new-session');
    expect(location.pathname).toMatch('/new-session');
  }, 10000);
});
