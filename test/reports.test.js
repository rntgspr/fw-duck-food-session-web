/* eslint-disable no-undef */
import '@babel/polyfill';
import { endpoint, screenshotsDir } from '../src/config';

describe('Reports', () => {
  beforeAll(async () => {
    await page.setViewport({ width: 375, height: 748, });
  });

  it('Should return the reports page', async () => {
    await page.goto(`${endpoint}/reports`, { waitUntil: 'networkidle0', });
    await page.screenshot({ path: `${screenshotsDir}/reports.0.jpg`, });

    const textToMath = 'Last Reports';
    const h1 = await page.evaluate(() => {
      return Array.from(document.getElementsByTagName('h1')).map(a => a.innerHTML);
    });

    await expect(h1[0]).toMatch(textToMath);
  }, 10000);

  it('Should navigate from Reports page to Session page', async () => {
    await page.goto(`${endpoint}/reports`, { waitUntil: 'networkidle0', });
    await page.click('a[href="/new-session"]');

    const location = await page.evaluate(() => {
      return document.location;
    });

    expect(location.href).toContain('/new-session');
    expect(location.pathname).toMatch('/new-session');
  }, 10000);

  it('Should navigate from Reports page to Home', async () => {
    await page.goto(`${endpoint}/reports`);
    await page.click('a[href="/"]');

    const location = await page.evaluate(() => {
      return document.location;
    });

    expect(location.href).toContain('/');
    expect(location.pathname).toMatch('/');
  }, 10000);
});
