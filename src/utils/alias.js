module.exports = {
  assets: './static/assets',
  client: './src/client',
  components: './src/components',
  data: './src/data',
  server: './src/server',
  serverless: './src/serverless',
  styles: './src/styles',
  utils: './src/utils',
};
