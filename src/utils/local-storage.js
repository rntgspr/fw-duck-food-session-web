export function getFromLocalStorage(tag) {
  if (global.localStorage?.getItem) return global.localStorage.getItem(tag);
  return '';
}

export function setInLocalStorage(tag, value) {
  if (global.localStorage?.setItem) return global.localStorage.setItem(tag, value);
  return getFromLocalStorage(tag);
}

export function removeFromLocalStorage(tag) {
  if (global.localStorage?.removeItem) global.localStorage.removeItem(tag);
}

export default null;
