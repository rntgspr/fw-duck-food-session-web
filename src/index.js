import React from 'react';
import ReactDOM from 'react-dom';

import HotClient from './client';

const APP_MOUNT_POINT = global.document.getElementById('AppMountPoint');
ReactDOM.render(<HotClient />, APP_MOUNT_POINT);
