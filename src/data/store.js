import { applyMiddleware, compose, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { routerMiddleware } from 'connected-react-router';
import { createBrowserHistory, createMemoryHistory } from 'history';

import allReducersWithHistory from 'data/reducers';

import hydrate from './hydrate';

/**
 * define if memory history, for isomorphic render, or browser history for client render;
 * */
export const history = !process.env.SERVER ? createBrowserHistory() : createMemoryHistory();

/**
 * configuring the store
 * */
export default (preloadedState = hydrate()) => {
  const store = createStore(
    allReducersWithHistory(history),
    preloadedState,
    compose(applyMiddleware(routerMiddleware(history), thunkMiddleware))
  );

  return store;
};
