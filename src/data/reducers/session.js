import { LOADING_SESSION, SUCCESS_SESSION, CLEAR_SESSION, ERROR_SESSION } from 'data/actions/session';

const initialState = {
  loaded: false,
  loading: false,
  data: null,
};

export default function(state = initialState, action = {}) {
  switch (action.type) {
    case LOADING_SESSION:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case SUCCESS_SESSION:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload.data,
      };
    case CLEAR_SESSION:
      return {
        ...initialState,
      };
    case ERROR_SESSION:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.error,
      };
    default:
      return state;
  }
}
