import { LOADING_CONTENT, LOADED_CONTENT, ERROR_CONTENT } from 'data/actions/content';

const initialState = {
  loaded: false,
  loading: false,
  result: {},
};

export default function(state = initialState, action = {}) {
  switch (action.type) {
    case LOADING_CONTENT:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case LOADED_CONTENT:
      return {
        ...state,
        loading: false,
        loaded: true,
        result: action.result,
      };
    case ERROR_CONTENT:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.error,
      };
    default:
      return state;
  }
}
