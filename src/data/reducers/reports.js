import { LOADING_REPORTS, SUCCESS_REPORTS, ERROR_REPORTS } from 'data/actions/reports';

const initialState = {
  loaded: false,
  loading: false,
};

export default function(state = initialState, action = {}) {
  switch (action.type) {
    case LOADING_REPORTS:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case SUCCESS_REPORTS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload?.data,
      };
    case ERROR_REPORTS:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.error,
      };
    default:
      return state;
  }
}
