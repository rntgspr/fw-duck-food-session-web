import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import content from './content';
import food from './food';
import session from './session';
import reports from './reports';

const rootReducer = history =>
  combineReducers({
    router: connectRouter(history),
    content,
    food,
    session,
    reports,
  });

export default rootReducer;
