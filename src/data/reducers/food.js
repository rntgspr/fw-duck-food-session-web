import { LOADING_FOOD, SUCCESS_FOOD, ERROR_FOOD } from 'data/actions/food';

const initialState = {
  loaded: false,
  loading: false,
  data: null,
};

export default function(state = initialState, action = {}) {
  switch (action.type) {
    case LOADING_FOOD:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case SUCCESS_FOOD:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload.data,
      };
    case ERROR_FOOD:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.error,
      };
    default:
      return state;
  }
}
