import GeneralClient from 'data/api/general-client';

export const LOADING_CONTENT = 'content/LOADING_CONTENT';
export const LOADED_CONTENT = 'content/LOADED_CONTENT';
export const ERROR_CONTENT = 'content/ERROR_CONTENT';

function loadingContent() {
  return { type: LOADING_CONTENT, };
}

function loadedContent(payload) {
  return {
    type: LOADED_CONTENT,
    result: payload,
  };
}

function failContent(error) {
  return { type: ERROR_CONTENT, error, };
}

export function loadContent(lang) {
  return async dispatch => {
    dispatch(loadingContent());
    try {
      const payload = await GeneralClient.getContent(lang);
      return dispatch(loadedContent(payload));
    } catch (error) {
      return dispatch(failContent(error.response.data));
    }
  };
}
