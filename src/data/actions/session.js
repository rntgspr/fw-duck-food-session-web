import GeneralClient from 'data/api/general-client';

export const LOADING_SESSION = 'session/LOADING_SESSION';
export const SUCCESS_SESSION = 'session/SUCCESS_SESSION';
export const CLEAR_SESSION = 'session/CLEAR_SESSION';
export const ERROR_SESSION = 'session/ERROR_SESSION';

function loadingSession() {
  return { type: LOADING_SESSION, };
}

function successSession(payload) {
  return { type: SUCCESS_SESSION, payload, };
}

function failSession(error) {
  return { type: ERROR_SESSION, error, };
}

export function sendSession(...params) {
  return async dispatch => {
    dispatch(loadingSession());
    try {
      const payload = await GeneralClient.sendSession(...params);
      return dispatch(successSession(payload));
    } catch (error) {
      return dispatch(failSession(error));
    }
  };
}

export function clearSession() {
  return { type: CLEAR_SESSION, };
}
