import GeneralClient from 'data/api/general-client';

export const LOADING_REPORTS = 'reports/LOADING_REPORTS';
export const SUCCESS_REPORTS = 'reports/SUCCESS_REPORTS';
export const ERROR_REPORTS = 'reports/ERROR_REPORTS';

function loadingReports() {
  return { type: LOADING_REPORTS, };
}

function successReports(payload) {
  return { type: SUCCESS_REPORTS, payload, };
}

function failReports(error) {
  return { type: ERROR_REPORTS, error, };
}

export function loadReports(...params) {
  return async dispatch => {
    dispatch(loadingReports());
    try {
      const payload = await GeneralClient.listFoodSessions(...params);
      return dispatch(successReports(payload));
    } catch (error) {
      return dispatch(failReports(error));
    }
  };
}
