import GeneralClient from 'data/api/general-client';

export const LOADING_FOOD = 'food/LOADING_FOOD';
export const SUCCESS_FOOD = 'food/SUCCESS_FOOD';
export const ERROR_FOOD = 'food/ERROR_FOOD';

function loadingFood() {
  return { type: LOADING_FOOD, };
}

function successFood(payload) {
  return { type: SUCCESS_FOOD, payload, };
}

function failFood(error) {
  return { type: ERROR_FOOD, error, };
}

export function loadFood(...params) {
  return async dispatch => {
    dispatch(loadingFood());
    try {
      const payload = await GeneralClient.getFoodOptions(...params);
      return dispatch(successFood(payload));
    } catch (error) {
      return dispatch(failFood(error));
    }
  };
}
