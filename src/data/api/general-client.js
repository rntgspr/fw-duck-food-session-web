import axios from 'axios';
import createFoodSessions from './create-food-sessions.graphql';
import queryEnumValues from './query-enum-values.graphql';
import queryUsingFedMomentsIndex from './query-using-fed-moments-index.graphql';
import { apiKey, apiUrl } from '../../config';

class GeneralClient {
  config = {
    content: {
      url: lang => `./content-${lang}.json`,
    },
    api: {
      url: apiUrl,
      headers: {
        'x-api-key': apiKey,
      },
    },
  };

  async getContent(lang = 'en_US') {
    const result = await axios.get(this.config.content.url(lang));
    return { ...result.data, };
  }

  async getFoodOptions() {
    const result = await axios({
      method: 'POST',
      headers: this.config.api.headers,
      url: this.config.api.url,
      data: { query: queryEnumValues, },
    });

    Object.keys(result.data.data).forEach(item => {
      result.data.data[item] = result.data.data[item].enumValues.map(i => i.name);
    });

    return { ...result.data, };
  }

  async sendSession(data) {
    const variables = {
      session: {
        ...data,
      },
    };

    const result = await axios({
      method: 'POST',
      headers: this.config.api.headers,
      url: this.config.api.url,
      data: { query: createFoodSessions, variables, },
    });

    return { ...result.data, };
  }

  async listFoodSessions(first = 9) {
    const variables = {
      first,
    };

    const result = await axios({
      method: 'POST',
      headers: this.config.api.headers,
      url: this.config.api.url,
      data: { query: queryUsingFedMomentsIndex, variables, },
    });

    return { ...result.data, };
  }
}

export default new GeneralClient();
