import { connect } from 'react-redux';

import { loadReports } from 'data/actions/reports';

import Reports from './reports';

function mapStateToProps(state, ownProps) {
  return {
    items: state.reports.data?.queryUsingFedMomentIndex?.items || [],
    ...ownProps,
  };
}

function mapDispatchToProps(dispatch) {
  return { loadReports: (...params) => dispatch(loadReports(...params)), };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reports);
