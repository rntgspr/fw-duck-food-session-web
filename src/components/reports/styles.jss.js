import { auto } from 'sagira-jss';
import { registerStyle } from 'styles';
// import { color } from 'styles/variables';

const styles = {
  reports: auto({ marginTop: 32, marginBottom: 64, }),
  list: auto({ marginTop: 32, marginBottom: 64, }),
  item: auto({ marginTop: 16, marginBottom: 16, }),
};

export default registerStyle(styles).classes;
