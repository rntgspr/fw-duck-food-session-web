import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { cn } from 'overclass';

import Loading from 'components/loading';
import withContext from 'client/context/context-hoc';

import classes from './styles.jss';

const Reports = ({ context, items, loadReports, }) => {
  const { LAST_REPORTS, } = context;
  useEffect(() => {
    loadReports(9);
  }, []);
  return (
    <div className={classes.reports}>
      {!items.length && <Loading />}
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <h1>{LAST_REPORTS}</h1>
          </div>
        </div>
        <ul className={classes.list}>
          {items.reverse().map(item => (
            <li key={item.id} className={cn`${classes.item} row`}>
              <span className="col-xs-10">{item.park}</span>
              <span className="col-xs-2">{item.totalDucks}</span>
              <small className="col-xs-5">{moment(item.fedMoment).format('LLL')}</small>
              <small className="col-xs-2">{context[(item.food?.kind)]}</small>
              <small className="col-xs-5">{context[(item.food?.type)]}</small>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

Reports.propTypes = {
  context: PropTypes.shape({}),
  items: PropTypes.arrayOf(
    PropTypes.shape({
      park: PropTypes.string,
      totalDucks: PropTypes.number,
      fedMoment: PropTypes.number,
      food: PropTypes.shape({
        kind: PropTypes.string,
        type: PropTypes.string,
      }),
    })
  ),
  loadReports: PropTypes.func,
};

Reports.defaultProps = {
  context: {},
  items: [],
  loadReports: null,
};

export default withContext(Reports);
