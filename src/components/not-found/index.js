import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const NotFound = ({ location, }) => (
  <div className="container">
    <br />
    <h1>404!</h1>
    <br />
    <p>location not found:</p>
    <h2>{location.pathname}</h2>
    <br />
    <h3>
      <Link to="/">home</Link>
    </h3>
  </div>
);

NotFound.propTypes = {
  location: PropTypes.shape({}),
};

NotFound.defaultProps = {
  location: {
    pathname: 'unknown',
  },
};

export default NotFound;
