import { auto } from 'sagira-jss';
import { registerStyle } from 'styles';
import { color } from 'styles/variables';

const styles = {
  loading: auto(
    {},
    {
      position: 'fixed',
      top: 0,
      left: 0,
      display: 'flex',
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: color.white,
      opacity: 0.97,
      zIndex: 3,
    }
  ),
};

export default registerStyle(styles).classes;
