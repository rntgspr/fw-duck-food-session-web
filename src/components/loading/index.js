import React from 'react';

import classes from './styles.jss';

const Loading = () => <div className={classes.loading}>Loading...</div>;

export default Loading;
