import { auto } from 'sagira-jss';
import { registerStyle } from 'styles';

const styles = {
  title: auto({ marginTop: 64, marginBottom: 64, }),
};

export default registerStyle(styles).classes;
