import { connect } from 'react-redux';

import { clearSession } from 'data/actions/session';
import EndSession from './end-session';

function mapStateToProps(state, ownProps) {
  return {
    session: state.session.data,
    ...ownProps,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    clearSession: () => dispatch(clearSession()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EndSession);
