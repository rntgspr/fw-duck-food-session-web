import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import withContext from 'client/context/context-hoc';

import classes from './styles.jss';

const EndSession = ({ context: { END_TITLE, SEE_REPORTS, FEED_DUCKS, }, session, clearSession, }) => {
  const [ titleFromSession, setTitleFromSession, ] = useState();

  useEffect(() => {
    const titleWithSessionData = (END_TITLE || '%d %p')
      .replace('%d', session?.createFoodSessions?.totalDucks || 0)
      .replace('%p', session?.createFoodSessions?.park || 'a park');

    setTitleFromSession(titleWithSessionData);
    clearSession();
  }, []);

  return (
    <div className={classes.endSession}>
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <h1 className={classes.title}>{titleFromSession}</h1>
            <Link className="button" to="/reports">
              {SEE_REPORTS}
            </Link>
            <Link className="button" to="/new-session">
              {FEED_DUCKS}
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

EndSession.propTypes = {
  context: PropTypes.shape({}),
  session: PropTypes.shape({}),
  clearSession: PropTypes.func,
};

EndSession.defaultProps = {
  context: {
    END_TITLE: '%d %p',
  },
  session: {
    createFoodSessions: {},
  },
  clearSession: null,
};

export default withContext(EndSession);
