import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import { sendSession } from 'data/actions/session';
import { loadFood } from 'data/actions/food';

import Session from './session';

function mapStateToProps(state, ownProps) {
  return {
    loaded: state.food.loaded || state.session.loaded,
    loading: state.food.loading || state.session.loading,
    session: state.session.data,
    food: state.food.data,
    ...ownProps,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sendSession: data => dispatch(sendSession(data)),
    loadFood: () => dispatch(loadFood()),
    endSession: () => dispatch(push('/end-session')),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Session);
