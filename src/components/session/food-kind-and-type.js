import React, { useState } from 'react';
import PropTypes from 'prop-types';

import withContext from 'client/context/context-hoc';
import classes from './styles.jss';

const SessionFoodKindAndType = ({ context, foodKind, foodTypeNatural, foodTypeRation, onChange, }) => {
  const { FOOD_AMOUNT, DEFINE_FOOD_KIND, DEFINE_FOOD_TYPE, } = context;
  const [ kind, setKind, ] = useState(undefined);
  const [ amount, setAmound, ] = useState(0);

  function changeAmount(e) {
    setAmound(e.target.value);
    onChange('foodAmount')(e, true);
  }

  function changeFoodKind(e) {
    setKind(e.target.value);
    onChange('foodKind')(e);
    onChange('foodType')({ target: { value: '', }, });
  }

  return (
    <div className={classes.kidAndType}>
      <h4>{FOOD_AMOUNT}</h4>
      <input name="foodAmount" type="number" step="100" value={amount} onChange={changeAmount} />
      <h4>{DEFINE_FOOD_KIND}</h4>
      <select name="foodKind" onChange={changeFoodKind}>
        <option />
        {foodKind.map(item => (
          <option key={item} value={item}>
            {context[item]}
          </option>
        ))}
      </select>
      {kind && `${DEFINE_FOOD_TYPE}:`}
      {kind === 'NOT_SURE' && (
        <select name="foodType" aria-hidden onChange={onChange('foodType')}>
          <option />
          {[ ...foodTypeRation, ...foodTypeNatural, ].map(item => (
            <option key={item} value={item}>
              {context[item]}
            </option>
          ))}
        </select>
      )}
      {kind === 'RATION' && (
        <select name="foodTypeRation" aria-hidden onChange={onChange('foodTypeRation')}>
          <option />
          {foodTypeRation.map(item => (
            <option key={item} value={item}>
              {context[item]}
            </option>
          ))}
        </select>
      )}
      {kind === 'NATURAL' && (
        <select name="foodTypeNatural" aria-hidden onChange={onChange('foodTypeNatural')}>
          <option />
          {foodTypeNatural.map(item => (
            <option key={item} value={item}>
              {context[item]}
            </option>
          ))}
        </select>
      )}
    </div>
  );
};

SessionFoodKindAndType.propTypes = {
  context: PropTypes.shape({}),
  foodKind: PropTypes.arrayOf(PropTypes.string),
  foodTypeNatural: PropTypes.arrayOf(PropTypes.string),
  foodTypeRation: PropTypes.arrayOf(PropTypes.string),
  onChange: PropTypes.func,
};

SessionFoodKindAndType.defaultProps = {
  context: {},
  foodKind: [],
  foodTypeNatural: [],
  foodTypeRation: [],
  onChange: null,
};

export default withContext(SessionFoodKindAndType);
