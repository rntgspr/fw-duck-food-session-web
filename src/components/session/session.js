import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Datetime from 'react-datetime';
import moment from 'moment';
import scroll from 'scroll';

import withContext from 'client/context/context-hoc';
import Loading from 'components/loading';

import SessionFoodKindAndType from './food-kind-and-type';
import classes from './styles.jss';

class Session extends Component {
  static propTypes = {
    context: PropTypes.shape({}),
    food: PropTypes.shape({}),
    loaded: PropTypes.bool,
    loading: PropTypes.bool,
    sendSession: PropTypes.func,
    endSession: PropTypes.func,
    session: PropTypes.shape({}),
  };

  static defaultProps = {
    context: {},
    food: null,
    loaded: false,
    loading: false,
    sendSession: null,
    endSession: null,
    session: null,
  };

  state = {
    park: '',
    totalDucks: 0,
    repeatDaily: false,
    fedMoment: moment(),
    food: {
      kind: '',
      type: '',
      amount: 0,
    },
    errors: [],
  };

  static getDerivedStateFromProps(props) {
    if (!props.loaded && !props.loading && props.food === null) {
      props.loadFood();
      return null;
    }

    if (props.loaded && props.session !== null) {
      props.endSession();
      return {
        park: '',
        totalDucks: 0,
        repeatDaily: false,
        fedMoment: moment(),
        food: {
          kind: '',
          type: '',
        },
        errors: [],
      };
    }

    return null;
  }

  _sendSessionHandler = () => {
    if (this._checkStateBeforeSend()) {
      const { sendSession, } = this.props;
      const sendState = { ...this.state, };
      sendState.fedMoment = parseInt(moment(sendState.fedMoment).format('x'), 10);
      delete sendState.errors;

      sendSession(sendState);
    }
  };

  _changeHandler = (tag, isInt) => e => {
    let nextState = null;
    if (e.target.type === 'checkbox') {
      nextState = e.target.checked;
    } else if (isInt) {
      nextState = Math.max(0, parseInt(e.target.value || 0, 10));
    } else {
      nextState = e.target.value;
    }

    this.setState({
      [tag]: nextState,
    });
  };

  _changeFedMomentHandler = date => {
    this.setState({
      fedMoment: date,
    });
  };

  _changeFoodTypeHandler = field => e => {
    const { food, } = this.state;
    const newFood = food;

    if (field === 'foodKind') {
      newFood.kind = e.target.value;
    } else if (field === 'foodAmount') {
      newFood.amount = parseInt(e.target.value, 10);
    } else if (field === 'foodTypeRation' || field === 'foodTypeNatural' || field === 'foodType') {
      newFood.type = e.target.value;
    }

    this.setState({
      food: newFood,
    });
  };

  _checkStateBeforeSend() {
    const newErrors = [];
    const { context, } = this.props;
    const { park, totalDucks, food, } = this.state;

    if (!park) {
      newErrors.push(context.ERROR_PARK);
    }

    if (totalDucks <= 0) {
      newErrors.push(context.ERROR_DUCKS);
    }

    if (food.amount <= 0) {
      newErrors.push(context.ERROR_FOOD_AMOUNT);
    }

    if (!food.kind) {
      newErrors.push(context.ERROR_FOOD_KIND);
    }

    if (!food.type) {
      newErrors.push(context.ERROR_FOOD_TYPE);
    }

    this.setState({ errors: newErrors, });
    if (newErrors.length) scroll.top(global.document.body, 0);

    return !newErrors.length;
  }

  render() {
    const { food, loading, context, } = this.props;
    const { errors, fedMoment, park, repeatDaily, totalDucks, } = this.state;
    return (
      <section className={classes.session}>
        {loading && <Loading />}

        <div className="container">
          {!!errors.length && (
            <div className="col-xs-12">
              <small>{context.CHECK_BEFORE_SUBMIT}</small>
              <ul className={classes.errorList}>
                {errors.map(item => (
                  <li key={item}>- {item}</li>
                ))}
              </ul>
            </div>
          )}

          <label htmlFor="park" className="row">
            <div className="col-xs-12 col-tv-6">
              <h3>{context.WICH_PARK}</h3>
            </div>
            <div className="col-xs-12 col-tv-6">
              <input name="park" type="text" onChange={this._changeHandler('park')} value={park} />
            </div>
          </label>

          <div className="label row">
            <div className="col-xs-12 col-tv-6">
              <h3>{context.WHEN_YOU_FED}</h3>
            </div>
            <div className="col-xs-12 col-tv-6 col-th-4">
              <Datetime name="fedMoment" onChange={this._changeFedMomentHandler} value={fedMoment} />
            </div>
          </div>

          <label htmlFor="repeatDaily" className="row">
            <div className="col-xs-10 col-tv-6">
              <h3>{context.REPEAT_TASK}</h3>
            </div>
            <div className="col-xs-2 col-tv-1">
              <input
                name="repeatDaily"
                type="checkbox"
                onChange={this._changeHandler('repeatDaily')}
                checked={repeatDaily}
              />
            </div>
          </label>

          <label htmlFor="repeatDaily" className="row">
            <div className="col-xs-12 col-tv-6">
              <h3>{context.HOW_MUCH_DUKCS}</h3>
            </div>
            <div className="col-xs-12 col-tv-6">
              <input
                name="totalDucks"
                type="number"
                step="3"
                unit="Ducks"
                onChange={this._changeHandler('totalDucks', true)}
                value={totalDucks.toString()}
              />
            </div>
          </label>

          <div className="label row">
            <div className="col-xs-12 col-tv-6">
              <h3>{context.WHAT_KIND}</h3>
            </div>
            <div className="col-xs-12 col-tv-6 col-th-4">
              <SessionFoodKindAndType {...food} onChange={this._changeFoodTypeHandler} />
            </div>
          </div>
        </div>
        <div className={classes.submit}>
          <div className="container">
            <div className={classes.buttons}>
              <button name="sendSession" onClick={this._sendSessionHandler} type="button">
                {context.SEND_SESSION}
              </button>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default withContext(Session);
