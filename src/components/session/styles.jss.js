import { auto } from 'sagira-jss';
import { registerStyle } from 'styles';
import { color } from 'styles/variables';

const styles = {
  session: auto({ paddingTop: 32, }),
  kidAndType: auto(
    {},
    {
      display: 'flex',
      flexDirection: 'column',
    }
  ),
  errorList: auto({ marginTop: 16, marginBottom: 32, }, { color: color.copperRed, }),
  submit: auto(
    {
      height: 42,
    },
    {
      position: 'sticky',
      bottom: 0,
      backgroundColor: color.copperRedDes,
      overflow: 'hidden',
    }
  ),
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
};

export default registerStyle(styles).classes;
