import { auto } from 'sagira-jss';
import { registerStyle } from 'styles';

const styles = {
  start: auto({ marginTop: 32, marginBottom: 128, }),
  title: auto({ marginBottom: 16, }),
};

export default registerStyle(styles).classes;
