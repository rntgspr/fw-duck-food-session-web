import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import withContext from 'client/context/context-hoc';

import classes from './styles.jss';

const Start = ({ context, }) => {
  const { FEED_DUCKS, SEE_REPORTS, WHAT_YOU_WANT_TO_DO, } = context;
  return (
    <div className={classes.start}>
      <h2 className={classes.title}>{WHAT_YOU_WANT_TO_DO}</h2>
      <Link className="button" to="/reports">
        {SEE_REPORTS}
      </Link>
      <Link className="button" to="/new-session">
        {FEED_DUCKS}
      </Link>
    </div>
  );
};

Start.propTypes = {
  context: PropTypes.shape({
    FEED_DUCKS: PropTypes.string,
    SEE_REPORTS: PropTypes.string,
    WHAT_YOU_WANT_TO_DO: PropTypes.string,
  }),
};

Start.defaultProps = {
  context: {
    FEED_DUCKS: '',
    SEE_REPORTS: '',
    WHAT_YOU_WANT_TO_DO: '',
  },
};

export default withContext(Start);
