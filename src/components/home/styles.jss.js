import { auto } from 'sagira-jss';
import { registerStyle } from 'styles';

const styles = {
  home: auto({ paddingTop: 96, }),
};

export default registerStyle(styles).classes;
