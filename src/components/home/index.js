import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { getFromLocalStorage, removeFromLocalStorage, setInLocalStorage } from 'utils/local-storage';
import withContext from 'client/context/context-hoc';
import Start from 'components/start';

import classes from './styles.jss';

const Home = ({ context: { I_AM_NOT, JUST_THE_FIRST_NAME, WELCOME, WELCOME_BACK, WHAT_IS_YOUR_NAME, }, }) => {
  const [ hasName, setHasName, ] = useState(!!getFromLocalStorage('name'));
  const [ name, setName, ] = useState(getFromLocalStorage('name'));
  useEffect(() => {
    if (name) setInLocalStorage('name', name);
  });

  function clearName() {
    removeFromLocalStorage('name');
    setName('');
    setHasName(false);
  }

  return (
    <section className={classes.home}>
      <div className="container">
        <div className="row">
          <div className="col-xs-12 col-tv-offset-5 col-tv-7 col-th-offset-6 col-th-6">
            {hasName ? (
              <React.Fragment>
                <h1>{`${WELCOME_BACK} ${name} !`}</h1>
                <hr />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <h1>{WELCOME}</h1>
                <div>{WHAT_IS_YOUR_NAME}</div>
                <input
                  name="name"
                  value={name || ''}
                  onChange={e => setName(e.target.value)}
                  placeholder={JUST_THE_FIRST_NAME}
                />
              </React.Fragment>
            )}
          </div>
          <div className="col-xs-12 col-tv-offset-5 col-tv-7 col-th-offset-6 col-th-6">
            <Start />
          </div>
          {name && <button className="button--link" type="button" onClick={clearName}>{`${I_AM_NOT} ${name}`}</button>}
        </div>
      </div>
    </section>
  );
};

Home.propTypes = {
  context: PropTypes.shape({}),
};

Home.defaultProps = {
  context: {
    LETS_GET_STARTED: '',
    LETS_GO: '',
    WELCOME: '',
    WELCOME_BACK: '',
    WHAT_IS_YOUR_NAME: '',
    JUST_THE_FIRST_NAME: '',
  },
};

export default withContext(Home);
