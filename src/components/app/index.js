import React from 'react';
import PropTypes from 'prop-types';

import Navigation from 'components/navigation';

const App = ({ children, }) => (
  <main>
    <Navigation />
    {children}
  </main>
);

App.propTypes = {
  children: PropTypes.node,
};

App.defaultProps = {
  children: null,
};

export default App;
