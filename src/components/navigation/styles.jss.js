import { auto } from 'sagira-jss';
import { registerStyle } from 'styles';
import { color } from 'styles/variables';

const styles = {
  nav: auto({ top: 0, height: 40, }, { backgroundColor: color.copperRedDes, position: 'sticky', zIndex: 2, }),
  list: auto(
    {},
    {
      display: 'flex',
    }
  ),
  item: auto({}),
};

export default registerStyle(styles).classes;
