import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Route, Switch, withRouter } from 'react-router';

import withContext from 'client/context/context-hoc';

import classes from './styles.jss';

const Navigation = ({ context: { BACK_HOME, FEED_DUCKS, REPORTS, }, location, }) => (
  <nav className={classes.nav}>
    <div className="container">
      <div className="row">
        <div className="col-xs-12">
          <ul className={classes.list}>
            <Switch location={location}>
              <Route exact path="/new-session">
                <React.Fragment>
                  <li className={classes.item}>
                    <Link className="button" to="/">
                      {BACK_HOME}
                    </Link>
                  </li>
                  <li className={classes.item}>
                    <Link className="button" to="/reports">
                      {REPORTS}
                    </Link>
                  </li>
                </React.Fragment>
              </Route>

              <Route exact path="/end-session">
                <React.Fragment>
                  <li className={classes.item}>
                    <Link className="button" to="/">
                      {BACK_HOME}
                    </Link>
                  </li>
                  <li className={classes.item}>
                    <Link className="button" to="/reports">
                      {REPORTS}
                    </Link>
                  </li>
                </React.Fragment>
              </Route>

              <Route exact path="/reports">
                <React.Fragment>
                  <li className={classes.item}>
                    <Link className="button" to="/">
                      {BACK_HOME}
                    </Link>
                  </li>
                  <li className={classes.item}>
                    <Link className="button" to="/new-session">
                      {FEED_DUCKS}
                    </Link>
                  </li>
                </React.Fragment>
              </Route>

              <Route exact path="/:any">
                <li className={classes.item}>
                  <Link className="button" to="/">
                    {BACK_HOME}
                  </Link>
                </li>
              </Route>
            </Switch>
          </ul>
        </div>
      </div>
    </div>
  </nav>
);

Navigation.propTypes = {
  context: PropTypes.shape({}),
  location: PropTypes.shape({}),
};

Navigation.defaultProps = {
  context: {},
  location: {},
};

export default withRouter(withContext(Navigation));
