import { vw, rem } from 'sagira-jss';

import jss from 'jss';
import jssNested from 'jss-nested';
import jssCamelCase from 'jss-camel-case';
import jssVendorPrefixer from 'jss-vendor-prefixer';
import jssGlobal from 'jss-global';

/**
 * Starting JSS
 */
jss.use(jssNested());
jss.use(jssCamelCase());
jss.use(jssGlobal());
jss.use(jssVendorPrefixer());

/**
 * Containers
 */
const containers = {
  xs: vw(338, 'xs'),
  ph: vw(376, 'ph'),
  tv: vw(704, 'tv'),
  th: rem(956),
  dk: rem(1170),
};

/**
 * Gaps on containers borders
 */
export const gaps = {
  xs: rem(5),
  ph: rem(4),
  tv: rem(8),
  th: rem(8),
  dk: rem(15),
};

/**
 * calculate negative gap
 */
export const negativeGap = breakpoint => {
  const compensation = containers[breakpoint];
  return `calc(((100vw - ${compensation}) / 2) * -1)`;
};

/**
 * calculate positive gap
 */
export const positiveGap = breakpoint => {
  const compensation = containers[breakpoint];
  return `calc((100vw - ${compensation}) / 2)`;
};

export const registerStyle = styles => jss.createStyleSheet(styles).attach();

export default null;
