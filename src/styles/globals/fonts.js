const fontOptions = {
  fonts: [
    {
      family: 'cabin-web',
      file: 'Cabin-Bold',
      weight: 'bold',
      style: 'bold',
    },
    {
      family: 'cabin-web',
      file: 'Cabin-Medium',
      weight: '500',
      style: 'normal',
    },
    {
      family: 'cabin-web',
      file: 'Cabin-Regular',
      weight: 'normal',
      style: 'normal',
    },
    {
      family: 'cabin-web',
      file: 'Cabin-SemiBold',
      weight: '300',
      style: 'normal',
    },
  ],
  prefix: '/fonts/',
};

export default fontOptions;
