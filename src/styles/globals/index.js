import Sagira from 'sagira-jss';

import jss from 'jss';
import jssNested from 'jss-nested';
import jssCamelCase from 'jss-camel-case';
import jssVendorPrefixer from 'jss-vendor-prefixer';
import jssGlobal from 'jss-global';

import fontOptions from './fonts';
import generalStyles from './general';

/**
 * Starting JSS
 */
jss.use(jssNested());
jss.use(jssCamelCase());
jss.use(jssGlobal());
jss.use(jssVendorPrefixer());

/**
 * Sagira Instance
 */
const sagira = new Sagira();
const flexboxGridStyleSheet = sagira.getFlexBoxStyleSheet();
const resetStyleSheet = sagira.getResetStyleSheet();
const fontsStyleSheet = sagira.getFontsStyleSheet(fontOptions);
const generalStyleSheet = jss.createStyleSheet(generalStyles);

/**
 * Attaching styles
 */
const GlobalStyles = () => {
  if (!resetStyleSheet.attached) resetStyleSheet.attach();
  if (!flexboxGridStyleSheet.attached) flexboxGridStyleSheet.attach();
  if (!fontsStyleSheet.attached) fontsStyleSheet.attach();
  if (!generalStyleSheet.attached) generalStyleSheet.attach();

  const result = [
    resetStyleSheet.toString(),
    flexboxGridStyleSheet.toString(),
    fontsStyleSheet.toString(),
    generalStyleSheet.toString(),
  ].join('\n');

  return result;
};

export default GlobalStyles();
