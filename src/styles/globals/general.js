import { auto, query, rem, vh } from 'sagira-jss';
import { color } from '../variables';

const reactDateTime = {
  table: {
    width: '100%',
  },
  td: auto({
    padding: 6,
  }),
  tr: auto({ height: 40, }, {}),
  '.form-control': {
    display: 'none',
  },
  '.rdtPrev': {},
  '.rdtNext': {},
  '.rdtSwitch': auto({ fontSize: 24, }),
  '.rdtPicker': {},
  '.rdtDays, .rdtMonths, .rdtYears': {},
  '.rdtDay': {},
  '.rdtOld': {
    opacity: 0.33,
  },
  '.rdtActive': {
    backgroundColor: color.copperRed,
    color: color.white,
  },
  '.rdtTime': {
    '& table': {
      width: '100%',
    },
  },
  '.rdtTimeToggle': auto(
    { fontSize: 32, },
    {
      textAlign: 'right',
    }
  ),
  '.rdtCounters': {
    display: 'flex',
    justifyContent: 'center',
  },
  '.rdtCounter': auto(
    {
      fontSize: 28,
      minWidth: 60,
      marginTop: 22,
      marginBottom: 22,
    },
    {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
    }
  ),
  '.rdtCounterSeparator': {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  '.rdtBtn': auto(
    {
      fontSize: 16,
    },
    {
      opacity: 0.34,
    }
  ),
};

const label = auto({ paddingTop: 16, paddingBottom: 32, });

const commomInputFields = auto(
  {
    fontSize: 14,
    minHeight: 40,
    padding: [ 10, ],
    marginBottom: 14,
  },
  {
    display: 'inline-flex',
    lineHeight: 'normal',
    color: color.black,
    border: `1px solid ${color.copperRed}`,
    backgroundColor: color.white,
    fontFamily: 'cabin-web',
  }
);

const simpleLink = auto(
  {
    padding: [ 4, 4, 4, 4, ],
    minHeight: 0,
    marginRight: 16,
  },
  {
    position: 'relative',
    backgroundColor: 'transparent',
    border: 'none',
    color: color.copperRed,
    '&::after': {
      content: "''",
      position: 'absolute',
      bottom: 0,
      left: 0,
      display: 'block',
      width: '100%',
      height: '1px',
      backgroundColor: color.copperRed,
    },
  }
);

const button = {
  ...commomInputFields,
  backgroundColor: color.copperRed,
  color: color.white,
  textAlign: 'center',
};

const titles = {
  color: color.copperRed,
  fontWeight: 'bold',
};

const styles = {
  '@global': {
    '*:focus': {
      outline: 0,
    },
    html: {
      fontFamily: 'cabin-web',
      '-webkit-font-smoothing': 'antialiased',
      '-moz-osx-font-smoothing': 'grayscale',
      backgroundColor: color.white,
      '@media only screen and (min-width: 1700px)': {
        fontSize: '113%',
      },
      '@media only screen and (min-width: 2400px)': {
        fontSize: '134%',
      },
    },
    a: {
      display: 'inline-block',
      position: 'relative',
      textDecoration: 'none',
    },
    p: {
      lineHeight: '1.66em',
      marginBottom: '1.66em',
      color: color.grey_6f,
      fontSize: vh(14),
      [query.ph]: {
        fontSize: vh(14, 'ph'),
      },
      [query.tv]: {
        fontSize: vh(14, 'tv'),
      },
      [query.th]: {
        fontSize: rem(16),
      },
    },
    blockquote: {},
    small: auto(
      { fontSize: 12, },
      {
        color: color.grey_4a,
      }
    ),
    h1: auto({ fontSize: 46, marginBottom: 22, }, titles),
    h2: auto({ fontSize: 36, marginBottom: 18, }, titles),
    h3: auto({ fontSize: 22, marginBottom: 12, }, titles),
    select: {
      ...commomInputFields,
    },
    label,
    input: {
      ...commomInputFields,
      width: '100%',
    },
    button,
    '.button': button,
    '.button--link': {
      ...simpleLink,
    },
    '.click': {
      cursor: 'pointer',
    },
    '.label': label,
    ...reactDateTime,
  },
};

export default styles;
