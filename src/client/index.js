import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { hot, setConfig } from 'react-hot-loader';

import configureStore, { history } from 'data/store';
import ContextProvider from 'client/context';

import ClientWithContext from './client';

/**
 * Starting lgobal styles
 */
import 'styles/globals';

/**
 * Configuring React hot loader
 */
setConfig({
  ignoreSFC: true,
  pureRender: true,
});

/**
 * Client start point
 */
class Client extends Component {
  constructor(...params) {
    super(...params);
    this.store = configureStore();
  }

  render() {
    return (
      <Provider store={this.store}>
        <ConnectedRouter history={history}>
          <ContextProvider>
            <ClientWithContext />
          </ContextProvider>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default (process.env.NODE_ENV === 'development' ? hot(module)(Client) : Client);
