import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router';
import { connect } from 'react-redux';

import App from 'components/app';
import Home from 'components/home';
import Session from 'components/session';
import Reports from 'components/reports';
import NotFound from 'components/not-found';
import EndSession from 'components/end-session';

class Client extends Component {
  static propTypes = {
    router: PropTypes.shape({
      location: PropTypes.oneOfType([ PropTypes.string, PropTypes.shape({ pathname: PropTypes.string, }), ]),
    }),
  };

  static defaultProps = {
    router: {},
  };

  state = {};

  render() {
    const { router, } = this.props;
    return (
      <App>
        <Switch location={router.location}>
          <Route exact path="/" component={Home} />
          <Route exact path="/new-session" component={Session} />
          <Route exact path="/end-session" component={EndSession} />
          <Route exact path="/reports" component={Reports} />
          <Route component={NotFound} />
        </Switch>
      </App>
    );
  }
}

export default connect(state => ({ router: state.router, }))(Client);
