import { connect } from 'react-redux';
import { loadContent } from 'data/actions/content';
import ContextProvider from './context-provider';

function mapStateToProps(state, ownProps) {
  return {
    content: state.content.result,
    ...ownProps,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadContent: () => dispatch(loadContent()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContextProvider);
