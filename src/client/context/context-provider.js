import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ContextRaw from './context-raw';

class ContextProvider extends Component {
  static propTypes = {
    children: PropTypes.node,
    content: PropTypes.shape({}),
    loadContent: PropTypes.func,
  };

  static defaultProps = {
    children: null,
    content: {},
    loadContent: null,
  };

  async componentDidMount() {
    const { loadContent, } = this.props;
    return loadContent();
  }

  render() {
    const { children, content, } = this.props;
    return <ContextRaw.Provider value={{ ...content, }}>{children}</ContextRaw.Provider>;
  }
}

export default ContextProvider;
