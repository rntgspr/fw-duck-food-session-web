import React from 'react';
import ContextRaw from './context-raw';

export default function withContext(Component) {
  return function WrapperComponent(props) {
    return <ContextRaw.Consumer>{state => <Component {...props} context={state} />}</ContextRaw.Consumer>;
  };
}
