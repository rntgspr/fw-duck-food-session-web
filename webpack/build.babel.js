import webpack from 'webpack';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';

import alias from '../src/utils/alias';

module.exports = {
  mode: 'production',
  entry: {
    client: [ '@babel/polyfill', './src/index.js', ],
  },
  resolve: {
    alias,
  },
  output: {
    publicPath: '/',
    filename: '[name].bundle.[hash].js',
    sourceMapFilename: 'map.map',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        enforce: 'pre',
        exclude: /node_modules/,
        loader: 'eslint-loader',
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.graphql?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'webpack-graphql-loader',
            options: {
              removeUnusedFragments: true,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new CopyWebpackPlugin([ { from: './public/', to: '../dist/', }, ]),
    new HtmlWebpackPlugin({
      template: './public/index.html',
      filename: './index.html',
    }),
  ],
};
