import path from 'path';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import alias from '../src/utils/alias';

module.exports = {
  devtool: 'inline-source-map',
  entry: {
    client: [ '@babel/polyfill', './src/index.js', ],
  },
  resolve: {
    alias,
  },
  output: {
    publicPath: 'http://localhost:3333/',
  },
  devServer: {
    port: 3333,
    contentBase: path.resolve(__dirname, '../public'),
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.graphql?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'webpack-graphql-loader',
            options: {
              removeUnusedFragments: true,
            },
          },
        ],
      },
      {
        test: /\.jsx?$/,
        include: /node_modules/,
        use: [ 'react-hot-loader/webpack', ],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: './public/index.html',
      filename: './index.html',
    }),
  ],
};
