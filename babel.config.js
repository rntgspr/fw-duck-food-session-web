const alias = require('./src/utils/alias');

module.exports = {
  babelrc: false,
  compact: false,
  presets: [ [ '@babel/preset-env', ], [ '@babel/preset-react', ], ],
  plugins: [
    [ 'react-hot-loader/babel', ],
    [ '@babel/plugin-proposal-class-properties', ],
    [ '@babel/plugin-proposal-optional-chaining', ],
    // [ '@babel/plugin-proposal-decorators', { legacy: true, }, ],
    // [ '@babel/plugin-proposal-export-namespace-from', ],
    // [ '@babel/plugin-proposal-function-sent', ],
    // [ '@babel/plugin-proposal-json-strings', ],
    // [ '@babel/plugin-proposal-numeric-separator', ],
    // [ '@babel/plugin-proposal-throw-expressions', ],
    [ '@babel/plugin-syntax-class-properties', ],
    // [ '@babel/plugin-syntax-dynamic-import', ],
    // [ '@babel/plugin-syntax-import-meta', ],
    [
      'module-resolver',
      {
        root: '.',
        alias,
      },
    ],
  ],
};
