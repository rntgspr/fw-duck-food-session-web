module.exports = {
  settings: {
    'import/parser': 'babel-eslint',
  },
  parser: 'babel-eslint',
  extends: ['eslint:recommended', 'plugin:jest/recommended', 'airbnb', 'prettier', 'prettier/react'],
  plugins: ['react', 'react-hooks', 'jsx-a11y', 'jest', 'prettier'],
  parserOptions: {
    ecmaFeatures: {
      blockBindings: true,
      jsx: true,
    },
  },
  rules: {
    'array-bracket-spacing': [2, 'always'],
    'comma-dangle': [2, { arrays: 'always', objects: 'always' }],
    'import/extensions': [0],
    'import/no-extraneous-dependencies': [0],
    'import/no-unresolved': [0],
    'jsx-quotes': [1, 'prefer-double'],
    'jsx-a11y/click-events-have-key-events': [0],
    'max-len': [
      1,
      120,
      4,
      { ignoreUrls: true, ignoreStrings: true, ignoreTemplateLiterals: true, ignoreRegExpLiterals: true },
    ],
    'no-debugger': [1],
    'no-param-reassign': [2, { props: true, ignorePropertyModificationsFor: ['result'] }],
    'no-underscore-dangle': [2, { allowAfterThis: true }],
    'react/jsx-uses-react': [1],
    'react/jsx-uses-vars': [1],
    'react/jsx-filename-extension': [0],
    'react/no-unused-prop-types': [0],
    'react-hooks/rules-of-hooks': [1],
  },
};
